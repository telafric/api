<?php
require_once('include/utils/utils.php');
require_once('modules/InterpayTransactions/InterpayTransactions.php');
require_once('modules/Users/Users.php');
require_once("config.php");
include_once 'include/database/PearDatabase.php';
include_once 'vtlib/Vtiger/Module.php';
require_once dirname(__FILE__) . '/includes/Loader.php';
include_once 'include/Webservices/SessionManager.php';
global $adb,$current_user;
$userid = 1;
$users = new Users();
$userdetails = $users->retrieveCurrentUserInfoFromFile($userid);
$current_user = $userdetails;


vimport ('includes.runtime.EntryPoint');
Vtiger_Session::set('AUTHUSERID', $userid);
$_SESSION['authenticated_user_id'] = $userid;
$_SESSION['authenticated_user_language'] = get_language($userid);
$_SESSION['AUTHUSERID'] = $userid;
$_SESSION['app_unique_key'] = $application_unique_key;


function get_language($id){
        global $adb;
    $adb = PearDatabase::getInstance();
    $result = $adb->pquery("SELECT * FROM vtiger_users WHERE id = ?",array($id));
    $language = $adb->query_result($result,0,'language');
    return $language;
}


//access token cheking
$headers = apache_request_headers();

//echo "<pre>";print_r($headers);die;


//$accessToken="fOS2ZP7JqzcGhN96";
$accessToken=$headers['authorization'];

$accessToken_existOrNot_sql="Select user_name from vtiger_users where accesskey=?";
$accessToken_existOrNot_result = $adb->pquery($accessToken_existOrNot_sql, array($accessToken));
$accessToken_existOrNot = $adb->num_rows($accessToken_existOrNot_result);

//echo "<pre>accesstoken=";print_r($accessToken_existOrNot);die;
//echo "<pre>";print_r($accessToken_existOrNot_result);die;

//echo "<pre>";print_r($accessToken);die;

if($accessToken_existOrNot){

//      echo "<pre>";print_r($_REQUEST);die;
                //Recieve data from  request
                $payerName=$_REQUEST['payerName'];
                $payerPhone=$_REQUEST['payerPhone'];
                $TansAmount=$_REQUEST['TansAmount'];
                $ownerPhone=$_REQUEST['ownerPhone'];
                $ownerName=$_REQUEST['ownerName'];
                $meterNumber=$_REQUEST['meterNumber'];
                $FeeTypeCode=$_REQUEST['FeeTypeCode'];
                $CurrencyCode=$_REQUEST['CurrencyCode'];
                $TxnDate=$_REQUEST['TxnDate'];
                $Txntime=$_REQUEST['Txntime'];
                $TransactionReferenceNumber=$_REQUEST['TransactionReferenceNumber'];
                $contactId="";
                if($FeeTypeCode=='GASTOPUP' || $FeeTypeCode=='SMARTCOOKER' || $FeeTypeCode=='INITIALDEPOSIT'){



                $payerPhonecheking_sql="SELECT  vtiger_contactdetails.contactid,vtiger_contactdetails.firstname,vtiger_contactdetails.lastname,vtiger_contactscf.* FROM vtiger_contactdetails
                        INNER JOIN vtiger_contactscf ON vtiger_contactdetails.contactid=vtiger_contactscf.contactid
                        INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_contactdetails.contactid
                        AND vtiger_crmentity.deleted=0
                        WHERE vtiger_contactdetails.mobile=? limit 1 ";
                        $payerPhone_result = $adb->pquery($payerPhonecheking_sql, array($payerPhone));
                        $payerPhone_existOrNot = $adb->num_rows($payerPhone_result);
                        //      echo "<pre>";print_r($payerPhone_existOrNot);die;

                        //if payer phone not exist in contact module thant check woner phone

                        if($payerPhone_existOrNot=="1"){

                                                foreach($payerPhone_result as $payer_details){


                                                $contactId=$payer_details["contactid"];
                                                break;
                                        }
                                        //echo "<pre>contact name=";print_r($contactName);die;

                        }else{


                                $WonerPhoneCking_sql="SELECT vtiger_contactdetails.contactid, vtiger_contactdetails.firstname,vtiger_contactdetails.lastname,vtiger_contactscf.* FROM vtiger_contactdetails
                                INNER JOIN vtiger_contactscf ON vtiger_contactdetails.contactid=vtiger_contactscf.contactid
                                INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_contactdetails.contactid
                                AND vtiger_crmentity.deleted=0
                                WHERE vtiger_contactdetails.mobile=? limit 1 ";
                                $wonerPhone_result = $adb->pquery($WonerPhoneCking_sql, array($ownerPhone));
                                $WonerPhone_existOrNot = $adb->num_rows($wonerPhone_result);

                                if($WonerPhone_existOrNot=="1"){


                                        foreach($wonerPhone_result as $woner_details){


                                                $contactId=$payer_details["contactid"];;
                                                break;
                                        }
                                }
                        }

                //      echo "<pre>contact name=";print_r($contactId);die;

                        if($contactId!=""){

                                //echo "save";
                                                        //Save Data into interpay tranjaction module
                                                        $focus = new InterpayTransactions();
                                                        $focus->column_fields['it_payername'] = $payerName;
                                                        $focus->column_fields['it_payerphone'] = $payerPhone;
                                                        $focus->column_fields['it_ownerphone'] = $ownerPhone;
                                                        $focus->column_fields['it_amount'] = $TansAmount;
                                                        $focus->column_fields['it_ownername'] = $ownerName;
                                                        $focus->column_fields['it_meternumber'] = $meterNumber;
                                                        $focus->column_fields['feetypecode'] = $FeeTypeCode;
                                                        $focus->column_fields['it_currencycode'] = $CurrencyCode;
                                                        $focus->column_fields['it_txn_date'] = $TxnDate;
                                                        $focus->column_fields['it_txn_time'] = $Txntime;
                                                        $focus->column_fields['it_trnumber'] = $TransactionReferenceNumber;
                                                        $focus->column_fields['cf_951'] = $contactId;

                                                        $focus->save("InterpayTransactions");

                                                        if($focus->id != '')
                                                        {
                                                           $con_id=$focus->id;
                                                        }

                                                        //$con_id=7;
                                                        $interpay_row_details="SELECT vtiger_interpaytransactions.*,vtiger_interpaytransactionscf.* FROM vtiger_interpaytransactions
                                                        INNER JOIN vtiger_interpaytransactionscf ON vtiger_interpaytransactions.interpaytransactionsid=vtiger_interpaytransactionscf.interpaytransactionsid
                                                        INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_interpaytransactions.interpaytransactionsid
                                                        AND vtiger_crmentity.deleted=0
                                                        WHERE vtiger_interpaytransactions.interpaytransactionsid=?";
                                                        $interpay_row_result = $adb->pquery($interpay_row_details, array($con_id));
                                                        $interpay_row = $adb->num_rows($interpay_row_result);

                                                        if($interpay_row>0){
                                                                $interpay_id_resultrow = $adb->fetch_array($interpay_row_result);
                                                        }

                                                        $interpay_details=array();
                                                        $interpay_details["success"] ="true";
                                                        $interpay_details["data"]["it_reco"]=$interpay_id_resultrow["interpaytransactionsid"];              $interpay_details["data"]["meterNumber"]=$interpay_id_resultrow["it_meternumber"];
                                                        $interpay_details["data"]["amount"]=$interpay_id_resultrow["it_amount"];
                                                        $interpay_details["data"]["payerName"]=$interpay_id_resultrow["it_payername"];
                                                        $interpay_details["data"]["payerPhone"]=$interpay_id_resultrow["it_payerphone"];
                        $interpay_details["data"]["paymentDateTime"]=$interpay_id_resultrow["it_txn_date"]." ".$interpay_id_resultrow["it_txn_time"];
                                                        $interpay_details["data"]["referenceNumber"]=$interpay_id_resultrow["it_trnumber"];
                                                        $interpay_details["data"]["currency"]=$interpay_id_resultrow["it_currencycode"];
                                                        $interpay_details["data"]["ownerName"]=$interpay_id_resultrow["it_ownername"];
                                                        $interpay_details["data"]["FeeTypeCode"]=$interpay_id_resultrow["feetypecode"];
                                                        $interpay_details["data"]["ownerPhone"]=$interpay_id_resultrow["it_ownerphone"];
                                                        $interpay_details["data"]["delaphoneContact"]=$interpay_id_resultrow["cf_951"];

                                                        //Succes Responce in json
                                                        $interpay_details_JSON = json_encode($interpay_details);
                                                        echo $interpay_details_JSON;


                                }//End of if contactId
                                else{
									
														//save record into database, delaphoneContact not save in this save 
										
														$focus = new InterpayTransactions();
                                                        $focus->column_fields['it_payername'] = $payerName;
                                                        $focus->column_fields['it_payerphone'] = $payerPhone;
                                                        $focus->column_fields['it_ownerphone'] = $ownerPhone;
                                                        $focus->column_fields['it_amount'] = $TansAmount;
                                                        $focus->column_fields['it_ownername'] = $ownerName;
                                                        $focus->column_fields['it_meternumber'] = $meterNumber;
                                                        $focus->column_fields['feetypecode'] = $FeeTypeCode;
                                                        $focus->column_fields['it_currencycode'] = $CurrencyCode;
                                                        $focus->column_fields['it_txn_date'] = $TxnDate;
                                                        $focus->column_fields['it_txn_time'] = $Txntime;
                                                        $focus->column_fields['it_trnumber'] = $TransactionReferenceNumber;
                                                        $focus->save("InterpayTransactions");
										
										
										
									
                                        //sent mail
                                        global $current_user,$HELPDESK_SUPPORT_EMAIL_ID, $HELPDESK_SUPPORT_NAME;
require_once("modules/Emails/mail.php");
$to_emailid='amit.mardana@jhunsinfotech.com,envirofit@delaphonegh.com';
$subject = 'Interpaytransaction Not updated';
$contents = '<html>

<table width="50%" border="0" cellpadding="2" cellspacing="2">
<tr>
        <td style="border:1px solid #f0f0f0;">Payer Phone</td>
        <td style="border:1px solid #f0f0f0;">'.$_REQUEST['payerPhone'].'</td>
</tr>
<tr>
        <td style="border:1px solid #f0f0f0;">Meter Number</td>
        <td style="border:1px solid #f0f0f0;">'.$_REQUEST['meterNumber'].'</td>
</tr>
<tr>
        <td style="border:1px solid #f0f0f0;">Amount(GHS)</td>
        <td style="border:1px solid #f0f0f0;">'.$_REQUEST['TansAmount'].'</td>
</tr>
<tr>
    <td style="border:1px solid #f0f0f0;">Payer Phone</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['payerPhone'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Owner Phone</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['ownerPhone'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Owner Name</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['ownerName'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Free Type Code</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['FeeTypeCode'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Currency Code</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['CurrencyCode'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Date</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['TxnDate'].'</td>
</tr>

<tr>
    <td style="border:1px solid #f0f0f0;">Time</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['Txntime'].'</td>
</tr>
<tr>
    <td style="border:1px solid #f0f0f0;">Interpay Trans Ref Number</td>
    <td style="border:1px solid #f0f0f0;">'.$_REQUEST['TransactionReferenceNumber'].'</td>
</tr>
</table>
</html>';
send_mail('Contacts', "$to_emailid", $HELPDESK_SUPPORT_NAME, $HELPDESK_SUPPORT_EMAIL_ID, $subject, $contents,'','','','','',true);



$sentmail["success"]="Mail sent successful";
$sentmail["data"]["meterNumber"]=$meterNumber;
$sentmail["data"]["amount"]=$TansAmount;
$sentmail["data"]["payerName"]=$payerName;
$sentmail["data"]["payerPhone"]=$payerPhone;
$sentmail["data"]["paymentDateTime"]=$TxnDate." ".$Txntime;
$sentmail["data"]["referenceNumber"]=$TransactionReferenceNumber;
$sentmail["data"]["currency"]=$CurrencyCode;
$sentmail["data"]["ownerName"]=$ownerName;
$sentmail["data"]["FeeTypeCode"]=$FeeTypeCode;
$sentmail["data"]["ownerPhone"]=$ownerPhone;

$interpay_details_JSON = json_encode($sentmail);
echo $interpay_details_JSON;


                                }

                        }//End of if Three condition Smartcooker,Gastopup,and INITIALDEPOSIT
                        else{
                                        /*
                                        $to_email = 'amit.mardana@jhunsinfotech.com';
                                        $subject = 'Interpay mail testing';
                                        $message = 'This testing mail';
                                        $headers = 'From:niiokai@delaphonegh.com';
                                        if(!mail($to_email,$subject,$message,$headers)){

                                                echo "Mail sent fail";
                                        }else{
                                                echo "Mail sent successful";

                                        }*/

                                        /*      global $current_user,$HELPDESK_SUPPORT_EMAIL_ID, $HELPDESK_SUPPORT_NAME;
                                                require_once("modules/Emails/mail.php");
                                                $to_emailid='amit.mardana@jhunsinfotech.com,envirofit@delaphonegh.com';
                                                $subject = 'test subject';
                                                $contents = '<html>

                                                <table width="50%" border="0" cellpadding="2" cellspacing="2">
                                                 <tr>
                                                    <td style="border:1px solid #f0f0f0;">Payer Phone</td><td style="border:1px solid #f0f0f0;">'.$_REQUEST['payerPhone'].'</td>
                                                </tr>
                                                <tr>
                                                   <td style="border:1px solid #f0f0f0;">Meter Number</td><td style="border:1px solid #f0f0f0;">'.$_REQUEST['meterNumber'].'</td>
                                                </tr>
                                                <tr>
                                                  <td style="border:1px solid #f0f0f0;">Amount(GHS)</td><td style="border:1px solid #f0f0f0;">'.$_REQUEST['TansAmount'].'</td>
                                                </tr>
                                                </table>
                                                </html>';
                                                send_mail('Contacts', "$to_emailid", $HELPDESK_SUPPORT_NAME, $HELPDESK_SUPPORT_EMAIL_ID, $subject, $contents,'','','','','',true);
                                                echo "Mail sent successful";
                                        */

                                        $InvalidFeeTypeCode["InvalidFeeTypeCode"]="FeeTypeCode is Invalid";
                                        $interpay_details_JSON = json_encode($InvalidFeeTypeCode);
                                        echo $interpay_details_JSON;


                                }

}else{

//echo "Acceess token Mismach";

$invalidToken["invalidToken"]="Acceess token Mismach";
$interpay_details_JSON = json_encode($invalidToken);
echo $interpay_details_JSON;

}

?>
