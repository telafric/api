<?php


	require_once('include/utils/utils.php');
	require_once('modules/InterpayTransactions/InterpayTransactions.php');
	require_once('modules/Users/Users.php');
	require_once("config.php");
	include_once 'include/database/PearDatabase.php';
	include_once 'vtlib/Vtiger/Module.php';
	require_once dirname(__FILE__) . '/includes/Loader.php';
	include_once 'include/Webservices/SessionManager.php';
	global $adb,$current_user;
	$userid = 1;
	$users = new Users();
	$userdetails = $users->retrieveCurrentUserInfoFromFile($userid);
	$current_user = $userdetails;

	vimport ('includes.runtime.EntryPoint');
	Vtiger_Session::set('AUTHUSERID', $userid);
	$_SESSION['authenticated_user_id'] = $userid;
	$_SESSION['authenticated_user_language'] = get_language($userid);
	$_SESSION['AUTHUSERID'] = $userid;
	$_SESSION['app_unique_key'] = $application_unique_key;


	function get_language($id){
		global $adb;
		$adb = PearDatabase::getInstance();
		$result = $adb->pquery("SELECT * FROM vtiger_users WHERE id = ?",array($id));
		$language = $adb->query_result($result,0,'language');
		return $language;
	}


	//Get access token
	$client_secret = "7MOg9zOMTknqcm0rENdnXQTOKI57EpRFjGye5sNA";
	$client_id = "6";
	$auth_url = "https://lpgadmin.envirofit.org/oauth/token";
	$grant_type="client_credentials";
	$post = "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret";
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $auth_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("cache-control: no-cache"));
	$response = curl_exec($curl);
	$json = json_decode($response, true);
	$access_token = $json['access_token'];



	//Get record from vtiget database
	$con_id=11;
	$interpay_row_details="SELECT vtiger_interpaytransactions.*,vtiger_interpaytransactionscf.* FROM vtiger_interpaytransactions
	INNER JOIN vtiger_interpaytransactionscf ON vtiger_interpaytransactions.interpaytransactionsid=vtiger_interpaytransactionscf.interpaytransactionsid
	INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_interpaytransactions.interpaytransactionsid
	AND vtiger_crmentity.deleted=0
	WHERE vtiger_interpaytransactions.interpaytransactionsid=?";
	$interpay_row_result = $adb->pquery($interpay_row_details, array($con_id));
	$interpay_row = $adb->num_rows($interpay_row_result);	
	if($interpay_row>0){
		$interpay_id_resultrow = $adb->fetch_array($interpay_row_result);
	}

	
	//Create varification code
	$interpay_date = $interpay_id_resultrow["it_txn_date"]." ".$interpay_id_resultrow["it_txn_time"];
	$pdate = preg_replace('/-|:|\s+/', null, $interpay_date);
	$amount = $interpay_id_resultrow["it_amount"]*100;
	$amount_date_string = $pdate.$amount;
	$md5 = md5($amount_date_string); 
	$verifCode = strtoupper(substr($md5,0,6));
	
	//sent data to envirofit
	$payment_url = "https://lpgadmin.envirofit.org/api/v1/clientPayments";
	$post_data = array(
		"verifCode"=>$verifCode,
		"currency"=>"GHS",
		"amount"=>$interpay_id_resultrow["it_amount"],
		"payerPhone"=>$interpay_id_resultrow["it_payerphone"],
		"paymentDateTime"=>$interpay_id_resultrow["it_txn_date"]." ".$interpay_id_resultrow["it_txn_time"],
		"referenceNumber"=>$interpay_id_resultrow["it_trnumber"],
		"payerName"=>$interpay_id_resultrow["it_payername"],
		"meterNumber"=>$interpay_id_resultrow["it_meternumber"]
	);

	$json = json_encode($post_data);
	$curl2 = curl_init();
	curl_setopt($curl2, CURLOPT_URL, $payment_url);
	curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl2, CURLOPT_POST, 1);
	curl_setopt($curl2, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl2, CURLOPT_HTTPHEADER, array(
		"content-type: application/json",
		"authorization: Bearer $access_token",
		"cache-control: no-cache"
	));

	$response = curl_exec($curl2);//Get responce
