<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/



function InterpayTransactions_AfterSaveSentdataToEnvirofit($entityData){
        $adb = PearDatabase::getInstance();
        $moduleName = $entityData->getModuleName();
        $wsId = $entityData->getId();
        $parts = explode('x', $wsId);
        $entityId = $parts[1];//last insert id or record id
        $entityDelta = new VTEntityDelta();
    $portalChanged = $entityDelta->hasChanged($moduleName, $entityId, 'portal');



        $interpay_row_details="SELECT vtiger_interpaytransactions.*,vtiger_interpaytransactionscf.* FROM vtiger_interpaytransactions
        INNER JOIN vtiger_interpaytransactionscf ON vtiger_interpaytransactions.interpaytransactionsid=vtiger_interpaytransactionscf.interpaytransactionsid
        INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid=vtiger_interpaytransactions.interpaytransactionsid
        AND vtiger_crmentity.deleted=0
        WHERE vtiger_interpaytransactions.interpaytransactionsid=?";
        $interpay_row_result = $adb->pquery($interpay_row_details, array($entityId));
        $interpay_row = $adb->num_rows($interpay_row_result);
        if($interpay_row>0){
                $interpay_id_resultrow = $adb->fetch_array($interpay_row_result);
        }

        if($interpay_id_resultrow["feetypecode"]=="GASTOPUP"){


        //Get access token
        $client_secret = "7MOg9zOMTknqcm0rENdnXQTOKI57EpRFjGye5sNA";
        $client_id = "6";
        $auth_url = "https://lpgadmin.envirofit.org/oauth/token";
        $grant_type="client_credentials";
        $post = "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $auth_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("cache-control: no-cache"));
        $response = curl_exec($curl);
        $json = json_decode($response, true);
        $access_token = $json['access_token'];


        //Create varification code
		///$interpay_date = $interpay_id_resultrow["it_txn_date"]." ".$interpay_id_resultrow["it_txn_time"];
        $interpay_date = $interpay_id_resultrow["it_txn_date"]." 00:30:00";
        $pdate = preg_replace('/-|:|\s+/', null, $interpay_date);
        $amount = $interpay_id_resultrow["it_amount"]*100;
        $amount_date_string = $pdate.$amount;
        $md5 = md5($amount_date_string);
        $verifCode = strtoupper(substr($md5,0,6));

        //sent data to envirofit
        $payment_url = "https://lpgadmin.envirofit.org/api/v1/clientPayments";
        $post_data = array(
                "verifCode"=>$verifCode,
                "currency"=>"GHS",
                "amount"=>$interpay_id_resultrow["it_amount"],
                "payerPhone"=>$interpay_id_resultrow["it_payerphone"],
                "paymentDateTime"=>$interpay_id_resultrow["it_txn_date"]." 00:30:00",
                "referenceNumber"=>$interpay_id_resultrow["it_trnumber"],
                "payerName"=>$interpay_id_resultrow["it_payername"],
                "meterNumber"=>$interpay_id_resultrow["it_meternumber"]
        );

        $json = json_encode($post_data);
        $curl2 = curl_init();
        curl_setopt($curl2, CURLOPT_URL, $payment_url);
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl2, CURLOPT_POST, 1);
        curl_setopt($curl2, CURLOPT_POSTFIELDS, $json);
        curl_setopt($curl2, CURLOPT_HTTPHEADER, array(
                "content-type: application/json",
                "authorization: Bearer $access_token",
                "cache-control: no-cache"
        ));

        //$response = curl_exec($curl2);//Get responce
        $response_json = curl_exec($curl2);//Get responce
        $response_array=json_decode($response_json);
        foreach($response_array as $key=>$res){

                if($key=="error"){


                        $evi_response="error";

                }

                if($key=="success"){

                        $evi_response="success";

                }
                break;
        }

        //Update table vtiger_interpaytransactionscf for envirofit responce
        $update_contact_sql="UPDATE vtiger_interpaytransactionscf SET cf_953=? WHERE vtiger_interpaytransactionscf.interpaytransactionsid=?";
        $adb->pquery($update_contact_sql, array($evi_response,$entityId));

        global $current_user,$HELPDESK_SUPPORT_EMAIL_ID, $HELPDESK_SUPPORT_NAME;
        require_once("modules/Emails/mail.php");
        $to_emailid='amit.mardana@jhunsinfotech.com,duke@delaphonegh.com';
        $subject = 'InterpayTransantion Testing';
        $contents = '<html>
                <table width="50%" border="0" cellpadding="7" cellspacing="0">
                  <tr>
                         <td style="border:1px solid #f0f0f0;">Payer Phone</td><td style="border:1px solid #f0f0f0;">'.$interpay_id_resultrow["it_payerphone"].'</td>
                </tr>
                <tr>
                 <td style="border:1px solid #f0f0f0;">Meter Number</td><td style="border:1px solid #f0f0f0;">'.$interpay_id_resultrow["it_meternumber"].'</td>
                 </tr>
                 <tr>
                 <td style="border:1px solid #f0f0f0;">Amount(GHS)</td><td style="border:1px solid #f0f0f0;">'.$interpay_id_resultrow["it_amount"].'</td>
                 </tr>
                </table>
                </html>';
                send_mail('Contacts', "$to_emailid", $HELPDESK_SUPPORT_NAME, $HELPDESK_SUPPORT_EMAIL_ID, $subject, $contents,'','','','','',true);




}// End of if condition

}//end of function

?>



        