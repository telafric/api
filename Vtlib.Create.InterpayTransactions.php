<?php
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
echo '<html><head><title>vtlib Module Script</title>';
echo '<style type="text/css">@import url("themes/softed/style.css");br { display: block; margin: 2px; }</style>';
echo '</head><body class=small style="font-size: 12px; margin: 2px; padding: 2px;">';
echo '<a href="index.php"><img src="themes/softed/images/vtiger-crm.gif" alt="vtiger CRM" title="vtiger CRM" border=0></a><hr style="height: 1px">'; 

$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = new Vtiger_Module();
$module->name = 'InterpayTransactions';
$module->save();

$module->initTables();
$module->initWebservice();

$menu = Vtiger_Menu::getInstance('Tools');
$menu->addModule($module);

$block1 = new Vtiger_Block();
$block1->label = 'LBL_INTERPAYTRANSACTIONS_INFORMATION';
$module->addBlock($block1);

/*BLOCK-1-START*/

$field1 = new Vtiger_Field();
$field1->name = 'it_code';
$field1->label = 'IT Code';
$field1->table = 'vtiger_interpaytransactions';
$field1->column = 'it_code';
$field1->columntype = 'VARCHAR(255)';
$field1->uitype = 4;
$field1->typeofdata = 'V~M';
$block1->addField($field1);


$field2 = new Vtiger_Field();
$field2->name = 'it_payername';
$field2->label = 'Payer Name ';
$field2->table = 'vtiger_interpaytransactions';
$field2->column = 'it_payername';
$field2->columntype = 'VARCHAR(255)';
$field2->uitype = 1;
$field2->typeofdata = 'V~M';
$block1->addField($field2);
$module->setEntityIdentifier($field2);

$field3 = new Vtiger_Field();
$field3->name = 'it_payerphone';
$field3->label = 'Payer Phone';
$field3->table = 'vtiger_interpaytransactions';
$field3->column = 'it_payerphone';
$field3->columntype = 'VARCHAR(255)';
$field3->uitype = 1;
$field3->typeofdata = 'V~M';
$block1->addField($field3);

$field4 = new Vtiger_Field();
$field4->name = 'it_ownername';
$field4->label = 'Owner Name';
$field4->table = 'vtiger_interpaytransactions';
$field4->column = 'it_ownername';
$field4->columntype = 'VARCHAR(255)';
$field4->uitype = 1;
$field4->typeofdata = 'V~O';
$block1->addField($field4);

$field5 = new Vtiger_Field();
$field5->name = 'it_ownerphone';
$field5->label = 'Owner Phone';
$field5->table = 'vtiger_interpaytransactions';
$field5->column = 'it_ownerphone';
$field5->columntype = 'VARCHAR(255)';
$field5->uitype = 1;
$field5->typeofdata = 'V~O';
$block1->addField($field5);

$field6 = new Vtiger_Field();
$field6->name = 'it_meternumber';
$field6->label = 'Meter Number';
$field6->table = 'vtiger_interpaytransactions';
$field6->column = 'it_meternumber';
$field6->columntype = 'VARCHAR(255)';
$field6->uitype = 1;
$field6->typeofdata = 'V~O';
$block1->addField($field6);

$field7 = new Vtiger_Field();
$field7->name = 'feetypecode';
$field7->label = 'Fee Type Code';
$field7->table = 'vtiger_interpaytransactions';
$field7->column = 'feetypecode';
$field7->columntype = 'VARCHAR(100)';
$field7->uitype = 15;
$field7->typeofdata = 'V~O';
$block1->addField($field7);
$field7->setpicklistvalues(array('SmartCooker', 'RefundDeposit', 'GasPayment'));


$field8 = new Vtiger_Field();
$field8->name = 'it_currencycode';
$field8->label = 'Currency Code';
$field8->table = 'vtiger_interpaytransactions';
$field8->column = 'it_currencycode';
$field8->columntype = 'VARCHAR(255)';
$field8->uitype = 1;
$field8->typeofdata = 'V~O';
$block1->addField($field8);


$field9 = new Vtiger_Field();
$field9->name = 'it_amount';
$field9->label = 'Amount';
$field9->table = 'vtiger_interpaytransactions';
$field9->column = 'it_amount';
$field9->columntype = 'VARCHAR(255)';
$field9->uitype = 1;
$field9->typeofdata = 'V~O';
$block1->addField($field9);

$field10 = new Vtiger_Field();
$field10->name = 'it_txn_date';
$field10->label = 'Txn Date';
$field10->table = 'vtiger_interpaytransactions';
$field10->column = 'it_txn_date';
$field10->columntype = 'DATE';
$field10->uitype = 5;
$field10->typeofdata = 'D~O';
$block1->addField($field10);

$field11 = new Vtiger_Field();
$field11->name = 'it_txn_time';
$field11->label = 'Txn Time';
$field11->table = 'vtiger_interpaytransactions';
$field11->column = 'it_txn_time';
$field11->columntype = 'TIME';
$field11->uitype = 14;
$field11->typeofdata = 'T~O';
$block1->addField($field11);

$field12 = new Vtiger_Field();
$field12->name = 'it_trnumber';
$field12->label = 'Interpay Trans Ref Number';
$field12->table = 'vtiger_interpaytransactions';
$field12->column = 'it_trnumber';
$field12->columntype = 'VARCHAR(255)';
$field12->uitype = 1;
$field12->typeofdata = 'V~O';
$block1->addField($field12);


/*COMMON FIELDS*/
$field100 = new Vtiger_Field();
$field100->name = 'assigned_user_id';
$field100->label = 'Assigned To';
$field100->table = 'vtiger_crmentity';
$field100->column = 'smownerid';
$field100->uitype = 53;
$field100->typeofdata = 'V~M';
$block1->addField($field100);

$field101 = new Vtiger_Field();
$field101->name = 'createdtime';
$field101->label= 'Created Time';
$field101->table = 'vtiger_crmentity';
$field101->column = 'createdtime';
$field101->uitype = 70;
$field101->typeofdata = 'T~O';
$field101->displaytype= 2;
$block1->addField($field101);

$field102 = new Vtiger_Field();
$field102->name = 'modifiedtime';
$field102->label= 'Modified Time';
$field102->table = 'vtiger_crmentity';
$field102->column = 'modifiedtime';
$field102->uitype = 70;
$field102->typeofdata = 'T~O';
$field102->displaytype= 2;
$block1->addField($field102);
/*COMMON FIELDS*/

/*BLOCK-1-END*/

//Filter
$filter1 = new Vtiger_Filter();
$filter1->name = 'All';
$filter1->isdefault = true;
$module->addFilter($filter1);

// Add fields to the filter created
$filter1->addField($field1)->addField($field2, 1)->addField($field3, 2)->addField($field4, 3)->addField($field5, 4)->addField($field6, 5);

//Reated modules
/*$module->setList(Vtiger_Module::getInstance('Products'), 'Products', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('Documents'), 'Documents', Array('ADD','SELECT'),'get_attachments');
$module->setList(Vtiger_Module::getInstance('Accounts'), 'Accounts', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('Contacts'), 'Contacts', Array('SELECT'),'get__list');
$module->setList(Vtiger_Module::getInstance('HelpDesk'), 'HelpDesk', Array('ADD'),'get_dependents_list');*/

//sharing access of this module
$module->setDefaultSharing('Public');

//Enable and Disable available tools
$module->enableTools(Array('Import', 'Export'));
$module->disableTools('Merge');

echo '</body></html>';
?>
