/**
	 * Function to get Contact related Interpay Transactions
	 * @param  integer   $id  - contactid
	 * returns related Interpay Transactions record in array format
	 */
	function get_interpayTransactions($id, $cur_tab_id, $rel_tab_id, $actions=false) { 
		global $log, $singlepane_view,$currentModule,$current_user;
		$log->debug("Entering get_interpayTransactions(".$id.") method ...");
		$this_module = $currentModule;
		
	    $related_module = vtlib_getModuleNameById($rel_tab_id);
		require_once("modules/$related_module/$related_module.php");
		$other = new $related_module();
        vtlib_setup_modulevars($related_module, $other);
		$singular_modname = vtlib_toSingular($related_module);

		$parenttab = getParentTab();

		if($singlepane_view == 'true')
			$returnset = '&return_module='.$this_module.'&return_action=DetailView&return_id='.$id;
		else
			$returnset = '&return_module='.$this_module.'&return_action=CallRelatedList&return_id='.$id;

		$button = '';

		if($actions) {
			if(is_string($actions)) $actions = explode(',', strtoupper($actions));
			if(in_array('SELECT', $actions) && isPermitted($related_module,4, '') == 'yes') {
				$button .= "<input title='".getTranslatedString('LBL_SELECT')." ". getTranslatedString($related_module). "' class='crmbutton small edit' type='button' onclick=\"return window.open('index.php?module=$related_module&return_module=$currentModule&action=Popup&popuptype=detailview&select=enable&form=EditView&form_submit=false&recordid=$id&parenttab=$parenttab','test','width=640,height=602,resizable=0,scrollbars=0');\" value='". getTranslatedString('LBL_SELECT'). " " . getTranslatedString($related_module) ."'>&nbsp;";
			}
			if(in_array('ADD', $actions) && isPermitted($related_module,1, '') == 'yes') {
				$button .= "<input title='".getTranslatedString('LBL_ADD_NEW'). " ". getTranslatedString($singular_modname) ."' class='crmbutton small create'" .
					" onclick='this.form.action.value=\"EditView\";this.form.module.value=\"$related_module\"' type='submit' name='button'" .
					" value='". getTranslatedString('LBL_ADD_NEW'). " " . getTranslatedString($singular_modname) ."'>&nbsp;";
			}
		}
 
		$query = 'SELECT vtiger_interpaytransactions.interpaytransactionsid, vtiger_interpaytransactions.it_payername, vtiger_interpaytransactions.it_code,
		 		  vtiger_interpaytransactions.it_meternumber, vtiger_interpaytransactions.feetypecode, vtiger_interpaytransactions.it_amount,
				  vtiger_crmentity.crmid, vtiger_crmentity.smownerid,vtiger_contactdetails.lastname
				FROM vtiger_interpaytransactions
				INNER JOIN vtiger_interpaytransactionscf
					ON vtiger_interpaytransactions.interpaytransactionsid = vtiger_interpaytransactionscf.interpaytransactionsid
				INNER JOIN vtiger_crmentity
					ON vtiger_crmentity.crmid = vtiger_interpaytransactions.interpaytransactionsid
				INNER JOIN vtiger_contactdetails
					ON vtiger_contactdetails.contactid = vtiger_interpaytransactionscf.cf_951
				LEFT JOIN vtiger_users
					ON vtiger_users.id=vtiger_crmentity.smownerid
				LEFT JOIN vtiger_groups
					ON vtiger_groups.groupid = vtiger_crmentity.smownerid
			   WHERE vtiger_contactdetails.contactid = '.$id.' and vtiger_crmentity.deleted = 0';

		$return_value = GetRelatedList($this_module, $related_module, $other, $query, $button, $returnset);

		if($return_value == null) $return_value = Array();
		$return_value['CUSTOM_BUTTON'] = $button;

		$log->debug("Exiting get_interpayTransactions method ...");
		return $return_value;  
	 }